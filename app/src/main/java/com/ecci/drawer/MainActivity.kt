package com.ecci.drawer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val navView = findViewById<NavigationView>(R.id.nav_view)
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        val navController = findNavController(R.id.nav_host_fragment)

        //Configuro mi propio toolbar
        setSupportActionBar(toolbar)

        //Que el TabBar se actualice según navego por la aplicación
        appBarConfiguration = AppBarConfiguration(setOf(R.id.blueFragment, R.id.greenFragment, R.id.redFragment), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        //Aquí configuramos para especificar que este navview va a usar ese navcontroller
        navView.setupWithNavController(navController)
    }

    // Este código es necesario para abrir el menú o mostrarme la flecha back
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}